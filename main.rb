require 'find'
require 'pathname'
require 'zip'
require 'json'

def upcase_keys(datas)
  datas.transform_keys do |key|
    key.to_s.upcase
  end
end

def generate_new_info(old_info)

  old_info.transform_values! do |value|
    if value.kind_of?(Array)
      value.join(', ')
    else
      value
    end
  end.map do |key, value|
    "#{key}: #{value}"
  end.join("\n")
end

def walk_info(input_path)
  Find.find(input_path) do |path|
    if !FileTest.directory?(path) && File.basename(path).end_with?('.zip')
      yield path
    end
  end
end

def replace_non_ascii(input)
  encoding_options = {
    invalid: :replace,  # Replace invalid byte sequences
    undef: :replace,  # Replace anything not defined in ASCII
    replace: '_'        # Use a blank for those replacements
  }

  input.encode(Encoding.find('ASCII'), **encoding_options)
end

def get_artist(filename)
  filename[/\[(.*?)\]/, 1]
end

def get_title(filename)
  filename.gsub('.zip', '')
end

def generate_info(path)
  filename = File.basename(path)

  {
    parsed_artist: [get_artist(filename)],
    parsed_title: [get_title(filename)]
  }
end

def add_info_zip(input_path)
  walk_info(input_path) do |path|
    puts path

    filename = File.basename(path)
  
    parsed_artist = get_artist(filename),
    parsed_title = get_title(filename)

    Zip::File.open(path, create: false, buffer: false) do |zip|
      temp = zip.read('info.json')
      existing_info = JSON.parse(temp) if temp
      existing_info = existing_info || {}

      existing_info['parsed_artist'] = parsed_artist unless existing_info['PARSED_ARTIST']
      existing_info['parsed_title'] = parsed_title unless existing_info['PARSED_TITLE']
      
      new_info = generate_new_info(upcase_keys(existing_info))
      File.write('info.txt', new_info)

      zip.add('info.txt', 'info.txt') {true}
    end

    rename_zip(path)

    File.delete('info.txt') if File.exist?('info.txt')
  end
end

def rename_zip(path)
  filename = File.basename(path)
  File.rename(path, File.join(File.dirname(path), replace_non_ascii(filename)))
end

# def rename_dir(input_path)
#   walk_info(input_path) do |path|
#     file = File.open(path, 'r')
#     info = read_new_info(file)
#     file.close
#
#     artist_string = info['ARTIST'].map{ |artist|  "[#{artist}]" }.join(" ")
#     f_id = info['FID'] && info['FID'][0]
#     title = info['TITLE'] && info['TITLE'][0]
#
#     ascii_filename = replace_non_ascii("#{artist_string} {#{f_id}} #{title}")
#
#     puts "name: #{ascii_filename}"
#
#     File.rename(File.dirname(file.path), File.join(File.dirname(File.dirname(file.path)), ascii_filename))
#   end
# end

input_path = ARGV[0] || '.'
puts 'start'
add_info_zip(input_path)
puts 'finished'



